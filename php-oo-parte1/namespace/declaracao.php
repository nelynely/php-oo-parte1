<?php

require_once '../src/Email/Envio.php';
require_once '../src/SMS/Envio.php';

use App\Email\Envio as Email;
use App\Email\Envio as SMS;
use const App\Email\VERSAO;
use function App\Email\Valida;

//arquivo sem namespace considera sempre como namespace global
$email = new Email();
$sms = new SMS();

echo valida('nelly.nf54@gmail.com') . "<br>";
echo VERSAO;

var_dump($email, $sms);