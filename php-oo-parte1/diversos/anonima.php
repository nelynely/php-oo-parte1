<?php

declare(strict_types=1);

require_once '../src/Classes/Fornecedor.php';

$fornecedor = new Fornecedor();

$fornecedor->autorizar(new class {
    public $nome = "James";
    public $senha = "3030";
});