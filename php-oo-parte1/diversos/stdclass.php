<?php

declare(strict_types=1);

$generic = new stdClass();

$generic->name = 'Treinaweb';
$generic->course = 'PHP OO';

var_dump($generic);

$array = [
    "nome" => "Treinaweb",
    "curso" => "PHP OO"
];

//transforma array em objeto
$objeto = (object) $array;

var_dump($objeto);