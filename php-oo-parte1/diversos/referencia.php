<?php

declare(strict_types=1);

require_once '../src/Classes/Produto.php';

$prod1 = new Produto();
$prod1->titulo = "Skol";
$prod1->descricao = "Cerveja Pilsen";
$prod1->preco = 2.50;

$prod2 = $prod1;
$prod2->preco = 3.40;

function alteraProduto(Produto $produto)
{
    $produto->titulo = "Skol Litrão";
}

alteraProduto($prod1);

$prod2 = 10;

var_dump($prod1, $prod2);

$prod3 = &$prod1;
$prod3->titulo = "Skol fininha";

$prod3 = 20;

var_dump($prod1, $prod3);
