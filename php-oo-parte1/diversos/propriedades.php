<?php

declare(strict_types=1);

require_once '../src/Classes/Produto.php';

$prod1 = new Produto();
$prod1->titulo = "Skol";
$prod1->descricao = "Cerveja Pilsen";
$prod1->preco = 2.50;
//propriedade em tempo de execução
$prod1->desconto = 0.1;

$prod1->defineCodigoBarras('0003030');
$prod1->acessaCodigoBarras();

var_dump($prod1);