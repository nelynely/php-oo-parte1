<?php

declare(strict_types=1);

require_once '../src/Classes/Fornecedor.php';

$fornecedor = new Fornecedor();

$fornecedor->cnpj = '04.651.096/0001-09';
$fornecedor->nomeFantasia = 'Laboratórios Teixeira';
$fornecedor->razaoSocial = 'Laboratórios Teixeira ltda';

$fornecedor->autorizar(new class {
    public $nome = 'James';
    public $senha = '3030';
});

var_dump($fornecedor);