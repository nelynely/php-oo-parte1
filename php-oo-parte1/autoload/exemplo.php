<?php

declare(strict_types=1);

require_once 'autoload.php';

$prod1 = new Produto();
$prod1->titulo = "Skol";
$prod1->descricao = "Cerveja Pilsen";
$prod1->preco = 2.50;

$cli = new Cliente();
$cli->nome = "James Gosling";
$cli->idade = 40;
$cli->endereco = "São Paulo";
$cli->telefone = "(11) xxxxx-xxxx";

$fornecedor = new Fornecedor();
$fornecedor->cnpj = '04.651.096/0001-09';
$fornecedor->nomeFantasia = 'Laboratórios Teixeira';
$fornecedor->razaoSocial = 'Laboratórios Teixeira ltda';

var_dump($prod1, $cli, $fornecedor);
