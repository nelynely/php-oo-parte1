<?php

namespace App\Classes;

class Fornecedor
{
    const  PAIS = "Brasil";

    public string $razaoSocial;

    public string $nomeFantasia;

    public string $cnpj;

    public function autorizar(object $usuario): void
    {
        if ($usuario->nome == "James" && $usuario->senha == "3030") {
            echo "<br> Autorizado: ";
        } else {
            echo "<br> Bloqueado: ";
        }
    }
}