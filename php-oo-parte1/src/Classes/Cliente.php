<?php

namespace App\Classes;

class Cliente
{
    public string $nome;

    public int $idade;

    public string $endereco;

    public string $telefone;

    public function __construct(string $nome, string $telefone, int $idade)
    {
        $this->nome = $nome;
        $this->telefone = $telefone;
        $this->idade = $idade;
    }

    public function comprar(): void
    {
        echo "O {$this->nome} realizou uma compra.";
    }

    public function defineNome(string $nome): void
    {
        $this->nome = $nome;
    }
}